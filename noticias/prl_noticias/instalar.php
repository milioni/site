<!--
/###############################################\
| Sistema de postagem de not�cias		|
| Vers�o 0.1a - Preludian project		|
|						|
| Desenvolvido por Jonhnatha Jorge 		|
| joepreludian@gmail.com			|
| http://users.boinc.ch/joepreludian		|
| Distribu�do sobre a licen�a GNU / GPL		|
| www.magnux.org/doc/GPL-pt_BR.txt		|
|						|
| Esta licen�a d� a voc� o direito de modificar |
| o conte�do desse script, entretanto deve man_ |
| ter os cr�ditos originais n�o removendo este	|
| cabe�alho. Obrigado e bom proveito :)		|
\###############################################/

-->

<?php 
require("cnf_config.php");
require("mod_funcoes.php");

if ($html_cod == 0){
$cod_html_c = "sim";
} elseif ($html_cod == 2) {
$cod_html_c = "n&atilde;o";
}


print "Cheque as informa&ccedil;&otilde;es antes de instalar<br>Fazendo leitura do arquivo \"/prl_noticias/cnf_config.php\"<br> Caso necess&aacute;rio edite o arquivo e em seguida rode esse script.<hr>
Servidor mysql: <b>" . varset("$mysql_server") ."</b> <br>
Base de dados mysql: <b>" . varset($mysql_db) ."</b><br>
Usu&aacute;rio mysql: <b>" . varset($mysql_username) . " </b><br>
Senha do usu&aacute;rio mysql: <b>" . varset($mysql_passwd) . "</b><br>
Permitir c&oacute;digo HTML: <b>" . varset($cod_html_c) . " </b><br>
Modificador da hora do servidor: <b>$alterador_hora hora(s)</b> <br>
Limite de posts na &iacute;ntegra: <b>" . varset($post_integra) . "</b> <br>
Limite de cabe&ccedil;alhos: <b>" . varset($post_cabecalho) . "</b> <br>
<hr>
<br><br>
<div align='center'><a href='act_instalar.php'>Tudo certo. INSTALAR!</a></div>";
?>
