<!--
/###############################################\
| Sistema de postagem de not�cias		|
| Vers�o 0.1a - Preludian project		|
|						|
| Desenvolvido por Jonhnatha Jorge 		|
| joepreludian@gmail.com			|
| http://users.boinc.ch/joepreludian		|
| Distribu�do sobre a licen�a GNU / GPL		|
| www.magnux.org/doc/GPL-pt_BR.txt		|
|						|
| Esta licen�a d� a voc� o direito de modificar |
| o conte�do desse script, entretanto deve man_ |
| ter os cr�ditos originais n�o removendo este	|
| cabe�alho. Obrigado e bom proveito :)		|
\###############################################/

-->

<?php

//fun��es prim�rias

function html($texto, $modo = 0){

$txt_clean_find = array("�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�");
$txt_clean_subst = array("&aacute;", "&eacute;", "&iacute;", "&oacute;", "&uacute;", "&ccedil;", "&atilde;", "&otilde;", "&acirc;", "&ecirc;", "&ocirc;", "&Aacute;", "&Eacute;", "&Iacute;", "&Oacute;", "&Uacute;", "&Ccedil;", "&Atilde;", "&Otilde;", "&Acirc;", "&Ecirc;", "&Ocirc;");

$txt_clean_find_t = array("�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "<", ">");
$txt_clean_subst_t = array("&aacute;", "&eacute;", "&iacute;", "&oacute;", "&uacute;", "&ccedil;", "&atilde;", "&otilde;", "&acirc;", "&ecirc;", "&ocirc;", "&Aacute;", "&Eacute;", "&Iacute;", "&Oacute;", "&Uacute;", "&Ccedil;", "&Atilde;", "&Otilde;", "&Acirc;", "&Ecirc;", "&Ocirc;","&lt;","&gt;");


if ($modo == 0){

return stripslashes(str_replace($txt_clean_find, $txt_clean_subst, $texto));
} 
if ($modo == 1) {
return stripslashes(str_replace($txt_clean_subst, $txt_clean_find, $texto));
}
if ($modo == 2) {
return stripslashes(str_replace($txt_clean_find_t, $txt_clean_subst_t, $texto));
}
}

function data_formatada($unix_time){
return strftime("%d/%m/%y - %I:%M %p", $unix_time);
}

$fuso_s = $alterador_hora * 3600;

function varset($var){

if ($var != "") {
return $var;
} else {
return "N&Atilde;O SETADA";
}
}

?>

