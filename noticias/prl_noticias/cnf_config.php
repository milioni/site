<!--
/###############################################\
| Sistema de postagem de not�cias		|
| Vers�o 0.1a - Preludian project		|
|						|
| Desenvolvido por Jonhnatha Jorge 		|
| joepreludian@gmail.com			|
| http://users.boinc.ch/joepreludian		|
| Distribu�do sobre a licen�a GNU / GPL		|
| www.magnux.org/doc/GPL-pt_BR.txt		|
|						|
| Esta licen�a d� a voc� o direito de modificar |
| o conte�do desse script, entretanto deve man_ |
| ter os cr�ditos originais n�o removendo este	|
| cabe�alho. Obrigado e bom proveito :)		|
\###############################################/

-->

<?php
//configura��o do sistema de news
//configura��es do servidor mysql

$mysql_server = "localhost";
$mysql_db = "configure aqui";
$mysql_username = "escolha seu usuario";
$mysql_passwd = "escolha ima senha aqui";

//permitir codigo html? pra caso da pessoa adicionar tags html na noticia..
$html_cod = "0"; //op��es disponiveis: "sim" -> 0 ou "n�o -> 2

/* alterador de hora: Esse recurso foi implementado para caso de voc� hospedar o site em servidores com hora baseado na GMT (greenwich alguma coisa com tempo =D hehehe) no caso, se tiver, basta colocar o variador para "-3". Caso o servidor seja brasileiro, ou possuir nosso fuso hor�rio, deixe em branco.*/

$alterador_hora = 0;

//limite de envio na integra. padr�o 1
$post_integra = 1;
//quantidade de posts 
$post_cabecalho = 4; 

//layouts, etc...

/* Esse � o molde que estar� saindo l� no site :) Deixei para voc� modificar ao seu gosto. Vc pode adicionar mais c�digos html, ou uma imagem, tabela... enfim... fica a seu crit�rio. A �nica coisa que 
vc n�o pode modificar s�o esses caracteres especiais 
(os "%s": eles que ser�o os caracteres especiais que colocar�o os valores das noticias) boa sorte!*/

$modelo_noticia_integra = "
<p align='center'><font color='000000' size='+1'>%s</font><br />
<font size='1'>Postado dia %s</font>

<p>%s</p>

<p align='center'><font color='666666' size='2'>Autor: %s  </font>
<br /><br />

";

// Posts - s� os links =D
//esse seria o cabe�alho dos links das news anteriores. Tome cuidado na ordem "Assunto e Data" :D
$modelo_posts_cabecalho = "
<table border='0' align='center'>
<tr>
<td>Assunto</td><td>Data</td><td>Exibi&ccedil;&otilde;es</td>
</tr>";

/* esse � o molde responsavel pelos itens das noticias. (o numero que tiver em "$post_cabe�alho" ser� o numero de registros que ele ir� repetir aqui.) preste aten��o aos caracteres "%s". Eles s�o os responsaveis por colocar os valores nos respectivos lugares. (sempre obedecendo a ordem de chegada...
por exemplo. O primeiro "%s" sera o numero da noticia, que vai ser criado um link <a>. O segundo "%s" � o nome do cabe�alho (ou titulo da new) e o terceiro � a data formatada. Os tres simbolos especiais precisam ser postos nessa ordem :) quanto ao conte�do, voc� pode modificar ao seu gosto

** tamb�m � muito importante deixar o link como est�... esta parte: 

*/
$modelo_posts_desenvolvimento = "<tr>
<td><a href='". $_SERVER["SCRIPT_NAME"] ."?l=prl_noticias/act_ver&id=%s'>%s</a></td><td>%s</td><td>%s</td>
</tr>";

//decidi por aqui caso queira adicionar mais alguma coisa...
$modelo_posts_dinal = "</table>";

?>